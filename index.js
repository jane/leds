import * as server from './server.js';
import * as lights from './lights.js';
import * as fs from 'fs';

const cfg = JSON.parse(fs.readFileSync('./config.json'));

server.recv(
  (res) => {
    lights.set_pattern(JSON.parse(res));
  },
  (res) => {
    console.log(`error callback: ${res}`);
  }
);

setInterval(() => {
  lights.tick();
}, cfg.sleep_time || 500);