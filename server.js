import * as net from 'net';
import * as fs from 'fs';
import Logger, { levels } from './logger.js';
import parse from './parse.js'

const cfg = JSON.parse(fs.readFileSync('./config.json'));
const log = new Logger("server", cfg.log_level ? levels[cfg.log_level] : levels.INFO);


const hostname = '0.0.0.0';
const port = 29999;

export function recv(callback, errorCallback) {
  let server = new net.Server();
  server.listen(port, hostname, () => {
    server.on('connection', (con) => {
      console.log('connection recieved: ' +
        con.remoteAddress + ":" + con.remotePort);
      // let data = [];
      // for (let key of Object.keys(functions)) {
      //   data.push({
      //     n: key.toUpperCase(),
      //     a: reqs[key.toLowerCase()] != undefined ? reqs[key.toLowerCase()] : true
      //   });
      // }
      // let functions_str = JSON.stringify(data);
      // con.write(functions_str);
      // log.debug(`sending ${functions_str}`);
      con.on('data', (data) => {
        let parsed_data = parse(String(data));

        if (parsed_data.errors != undefined && parsed_data.errors.length > 0) {
          con.write(`error ..\n${JSON.stringify(parsed_data)}`);
          errorCallback(JSON.stringify(parsed_data));
        }
        else {
          con.write(`success ..\n${JSON.stringify(parsed_data)}`);
          callback(JSON.stringify(parsed_data));
        }
      });
      con.on('close', () => {
        console.log('recieved close for ' +
          con.remoteAddress + ":" + con.remotePort);
        con.destroy();
      });

      server.getConnections((err, cons) => {
        if (err) {
          console.error(err);
        }
        else {
          console.log(`connections: ${cons}`);
        }
      })
    })

    server.on('error', (e) => {
      server.close();
      errorCallback(e);
    });
  });
}
