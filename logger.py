debug_statements = False


def debug(msg):
    if debug_statements:
        print(msg)


def info(msg):
    print(msg)
