#!/usr/bin/env python

#
# based on code from lrvick and LiquidCrystal
# lrvic - https://github.com/lrvick/raspi-hd44780/blob/master/hd44780.py
# LiquidCrystal - https://github.com/arduino/Arduino/blob/master/libraries/LiquidCrystal/LiquidCrystal.cpp
#

from time import sleep
import sys
import lcd_manager
import light_manager
import pattern
import logger
import threading
import socket

this = sys.modules[__name__]

this.level = 0
this.level_max = 14
this.level_state = 0
this.color_state = 0


def lightlevel(lcd, level):
    logger.debug("display level")
    lcd.clear()
    lcd.message("Light Level:\n]" + "-"*level + "[")


def query():
    level_state = 2


def color(lcd):
    lcd.clear()
    lcd.message("new pattern loaded.")
    logger.debug("NYI")


def get_state():
    if this.level_state > 0:
        this.level_state -= 1
        return "level"
    elif this.color_state > 0:
        this.color_state -= 1
        return "color"
    else:
        return "idle"


def displayon(lcd):
    if lcd.displaycontrol & lcd.LCD_DISPLAYON != lcd.displaycontrol:
        lcd.display()


def socket_loop():
    injected = False
    s = socket.create_server(('0.0.0.0', 29999))
    while True:
        if not injected:
            this.lights.set_pattern(pattern.pat)
            injected = True
        sock, addr = s.accept()
        with sock:
            length_data = sock.recv(4)
            logger.debug(length_data)
            length = int(length_data.decode())
            logger.debug(length)
            pattern_data = sock.recv(length)
            logger.debug(pattern_data)
            pattern.parse(pattern_data.decode())
            this.color_state = 7


def loop():
    socket_thread = threading.Thread(target=socket_loop)
    lcd = lcd_manager.Adafruit_CharLCD()
    this.lights = light_manager.LightStrip(string_length=450, brightness=0.6, max_changes=10)
    socket_thread.start()
    while True:
        logger.debug("loop")
        this.lights.tick()
        query()
        state = get_state()

        if state == "level":
            if this.lights.get_light_level() != (this.level / this.level_max):
                this.lights.set_light_level(this.level / this.level_max)
            lightlevel(lcd, this.level)
        elif state == "color":
            color(lcd)
        else:
            lcd.noDisplay()
        sleep(0.1)


if __name__ == '__main__':
    loop()
