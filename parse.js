import * as fs from 'fs';
import Logger, { levels } from './logger.js';
import { functions } from './lights.js';

const cfg = JSON.parse(fs.readFileSync('./config.json'));
const log = new Logger("server", cfg.log_level ? levels[cfg.log_level] : levels.INFO);


export default function parse(data) {
  let parsed = []
  let errors = []
  log.info(data);
  // errors.push("not yet implemented");
  data = JSON.parse(data);
  let line = data.split("\n");
  for (let lineNumber in line) {
    let currentLine = line[lineNumber];
    let split = currentLine.split(" ");
    let command = split[0];
    let arg1 = split[1];
    let arg2 = split[2];

    let match = functions[command];
    if (match != undefined) {
      if (
        (match.options["requires_arg1"] && arg1 == undefined) ||
        (match.options["requires_arg2"] && arg2 == undefined)
      ) {
        errors.push(`error parsing line ${lineNumber}, invalid number of args`);
      }
      else if (!match.options["convert_args"] && parseInt(arg1)) {
        errors.push(`error parsing line ${lineNumber}, argument ${arg1} cannot be a number`);
      }
      else {
        parsed.push(
          {
            command: command,
            arg1: arg1 || undefined,
            arg2: arg2 || undefined
          }
        );
      }
    }
  }
  return {
    parsed: parsed,
    data: data,
    errors: errors
  }
}